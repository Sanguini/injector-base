#include "Process.h"
#include "XorStr.hpp"
#include "ErrorLogger.h"
#include <TlHelp32.h>
#include <Psapi.h>
#include <fstream>

DWORD Process::findProc()
{
	if (procName.length() > 0)
	{
		PROCESSENTRY32 entry;
		entry.dwSize = sizeof(PROCESSENTRY32);

		HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
		if (Process32First(snapshot, &entry) == TRUE)
		{
			while (Process32Next(snapshot, &entry) == TRUE) //Loop through all the process
			{
				if (strcmp(entry.szExeFile, this->procName.c_str()) == 0)	//if the .exe name is the one we search
				{
					this->dProcID = entry.th32ProcessID + 1;	
					return entry.th32ProcessID;
				}
			}
		}
	}
	//MAYBE THE EXE NAME CHANGED
	if (windowName.length() > 0)
	{
		HWND cwnd = FindWindowA(NULL, this->windowName.c_str());
		DWORD tempPID = NULL;
		GetWindowThreadProcessId(cwnd, &tempPID);
		if (tempPID)
		{
			this->dProcID = tempPID + 1;
			return tempPID;
		}
	}

	//IF THERE IS NO PROCESS INVALIDATE THINGS
	this->dProcID = NULL;
	return NULL;
}

bool Process::inject(std::string dllName)
{
	//normally ac checks if any process with an handle to the protected process
	//so having an handle is like a hot potato you only want it as short as possible :P

	HANDLE hProcHandle = OpenProcess(PROCESS_ALL_ACCESS, false, this->dProcID - 1);			//Gets the handle to the Process
	if (hProcHandle == INVALID_HANDLE_VALUE)
	{
		eLog::logError(XorStr("Couldn't Open the Process"), XorStr("ERROR"));
		return false;
	}

	LPVOID llAddy = (LPVOID)GetProcAddress(GetModuleHandleA(XorStr("kernel32.dll")), XorStr("LoadLibaryA"));//Find the Loadlibrary function which will be written into the target proc

	if (llAddy == NULL)
	{
		eLog::logError(XorStr("LoadLibraryA function couldn't be found inside Kernel32.dll"), XorStr("ERROR"));
		return false;
	}

	LPVOID arg = (LPVOID)VirtualAllocEx(hProcHandle, NULL, dllName.size(), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);	//allocate enough memory that it can hold the dll name
	if (arg == NULL)
	{
		eLog::logError(XorStr("Memory couldn't be allocated into target process"), XorStr("ERROR"));
		return false;
	}

	int bytesWritten = WriteProcessMemory(hProcHandle, arg, dllName.c_str(), dllName.size(), NULL);	//Write Loadlibrary and the target name into the process
	if (bytesWritten == 0)
	{
		eLog::logError(XorStr("Couldn't write LoadLibrary into target process"), XorStr("ERROR"));
		return false;
	}

	HANDLE hThread = CreateRemoteThread(hProcHandle, NULL, 0, (LPTHREAD_START_ROUTINE)llAddy, arg, NULL, NULL);
	if (hThread == NULL)
	{
		eLog::logError(XorStr("Remote thread couldn't be created"), XorStr("ERROR"));
		return false;
	}

	eLog::logError(XorStr("DLL injected"), XorStr("SUCESS"));
	CloseHandle(hProcHandle);
	return true;
}

Process::Process(char * processName, char * windowName)
{
	this->windowName = XorStr(windowName);
	this->procName = XorStr(processName);
}

bool isLegitDll(std::string dllName)
{
	if (dllName.find("/") == std::string::npos)
		return false;	//we want it to be absolute
	//TODO ADD HEADER STUFF TO VERIFY LEGITNESS OF DLL
	std::ifstream input;	//just trust the user that it is a dll and they wont break stuff :P
	input.open(dllName);
	if (input.good() && !input.bad())
		return true;
	return false;
}

void Process::Tick()
{
	if (this->findProc())	//IF PROCESS IT THERE
	{
		if (this->sDllNameArray.empty())	//AND WE GOT AN DLL TO INJECT
			return;

		for (const auto& dll : this->sDllNameArray)
		{
			if(isLegitDll(dll))
				this->inject(dll);
			else {
				eLog::logError(dll + XorStr(" isn't a DLL"), XorStr("ERROR"));
			}
		}
	}
}

void Process::pushStringToInject(std::string dllName)
{
	this->sDllNameArray.push_back(dllName);
}

void Process::removeDllName(std::string dllName)
{

}
