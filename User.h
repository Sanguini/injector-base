#pragma once
#include <iostream>
#include "DllList.h"

class User : public DLLList
{
private:
	DWORD dUUID;
	bool verifyUUID();
	inline bool login(std::string usrname, std::string pass, DWORD randomStuff); //DONT WORRY ABOUT randomStuff
public:
	void onStartup(int argc, char** argv);	//WILL VERIFY UUID IF PASSED

};
