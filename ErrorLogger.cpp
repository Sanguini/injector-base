#include "ErrorLogger.h"
#include <fstream>
#include <ctime>

std::ofstream errorFile;

void eLog::initLogger(std::string errorFileName)
{
	errorFile.open(errorFileName, std::ios::app);
}

void eLog::logError(std::string errorTxt, std::string appendix, bool showTime)
{
	if (showTime)
	{
		time_t t = time(0);
		struct tm * now = localtime(&t);
		errorFile << "[" << now->tm_mday << "." << now->tm_mon << " / " << now->tm_hour << ":" << now->tm_min << "." << now->tm_sec << " ";
	}
	errorFile << "[" << appendix << "] "  << errorTxt << std::endl;
	errorFile.flush();
}

