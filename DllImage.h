#pragma once
#include <iostream>

class DllImage {
private:
	BYTE* downDll(DWORD uUID);
	bool saveDll(BYTE* dllArray);
public:
	int id;
	bool selectedState;
	std::string dllName;
	std::string authorName;
	std::string version;
	bool get(DWORD uUID) { return this->saveDll(this->downDll(uUID)); }
};