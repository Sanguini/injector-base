#pragma once
#include <iostream>
#include "XorStr.hpp"

namespace eLog
{
	void initLogger(std::string errorFileName = XorStr("error.txt"));
	void logError(std::string errorTxt, std::string appendix = "", bool showTime = true);
}
