#pragma once
#include <Windows.h>
#include <iostream>
#include <vector>

class Process {
private:
	std::string procName;	//.exe name to search for in running processes
	std::string windowName;	//Window title

	DWORD dProcID;	//Process ID

	DWORD findProc();	//Finds the process id either utilizing window or loop running processes
	bool inject(std::string dllName);

	std::vector<std::string> sDllNameArray;

	bool isReady = false;
public:
	Process(char* processName, char* windowName);
	void Tick();
	bool getReady() { this->isReady; }
	void pushStringToInject(std::string dllName);
	void removeDllName(std::string dllName);
};
