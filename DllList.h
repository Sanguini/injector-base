#pragma once
#include <iostream>
#include <vector>
#include "Process.h"
#include "DllImage.h"

class DLLList {
private:
	std::vector<Process> procs;
	std::vector<DllImage> dllImages;
public:
	void getAvailDllList(DWORD uUID);	//GETS LIST OF DLL'S POSSIBLE FOR INJECTION and fills them into dllImages
	void selectDll(int idx);	//GET PROC FOR DLL THEN LOOK IF PROC ALREADY THERE IF NOT ADD PROC THEN CHANGE HASH AND CHANGE NAME THEN ADD TO INJECTION NAME LIST
	void injectAll();			//ROTATES THROU ALL THE DLLS ROTATES THROU ALL THE PROCESSES CALLING INJECT		
};

